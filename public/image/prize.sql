-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 2017-03-15 08:06:48
-- 服务器版本： 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fengtian`
--

-- --------------------------------------------------------

--
-- 表的结构 `prize`
--

CREATE TABLE `prize` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) DEFAULT NULL COMMENT '设置名',
  `value` varchar(255) DEFAULT NULL COMMENT '设置值',
  `desc` varchar(255) DEFAULT NULL COMMENT '中文注释',
  `prize` varchar(30) NOT NULL COMMENT '奖品名称',
  `proab` int(11) NOT NULL COMMENT '概率',
  `type` varchar(20) NOT NULL COMMENT '奖品类型',
  `min_angle` varchar(50) NOT NULL COMMENT '最小角度',
  `max_angle` varchar(50) NOT NULL COMMENT '最大角度',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delected_at` timestamp NULL DEFAULT NULL,
  `act` int(11) NOT NULL COMMENT '是否是奖品'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='奖品表';

--
-- 转存表中的数据 `prize`
--

INSERT INTO `prize` (`id`, `name`, `value`, `desc`, `prize`, `proab`, `type`, `min_angle`, `max_angle`, `created_at`, `updated_at`, `delected_at`, `act`) VALUES
(1, '1', '300', '每天一等奖的数量', '1000元交强险基金', 200, '雷凌购车礼', '315', '355', NULL, '2017-03-09 03:59:57', NULL, 1),
(2, '2', '0', '每天二等奖的数量', '149元小米手环2', 0, '雷凌到店礼', '255', '290', NULL, NULL, NULL, 1),
(3, '2', '0', '每天三等奖的数量', '50元京东E卡', 0, '雷凌到店礼', '190', '235', NULL, NULL, NULL, 1),
(4, '2', '0', '每天四等奖的数量', '2888元ipad mini4', 0, '致享到店礼', '5', '50', NULL, NULL, NULL, 1),
(5, '2', '0', '每天五等奖的数量', '599元户外帐篷', 0, '致享到店礼', '130', '170', NULL, NULL, NULL, 1),
(6, '2', '0', '每天六等奖的数量', '没有奖', 0, '0', '75', '115', NULL, NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `prize`
--
ALTER TABLE `prize`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `prize`
--
ALTER TABLE `prize`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
