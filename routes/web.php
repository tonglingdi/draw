<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "IndexController@index");
Route::post('/start/{type}', "IndexController@start");
Route::post('/start', "IndexController@start");
Route::any('/write/info', "IndexController@info");
Route::any('/detail', "IndexController@detail");
Route::any('/prize/{start}', "IndexController@prize");
Route::any('/getcode', "IndexController@getcode");
Route::any('/city/{code}', 'IndexController@city');
Route::any('/shop/{code}', 'IndexController@shop');
Route::any('/sendcode/{code}', 'IndexController@sendcode');
Route::any('/check_tel', 'IndexController@check_tel');
Route::any('/handle_info', 'IndexController@handle_info');
Route::any('/send/sms',"IndexController@sendsms");
Route::any('/load', function (){
    return view("show.loading");
});

Route::any('/test/test',"TestController@test");
Route::any('/test/send',"TestController@sendcard");
Route::any('/test/sms',"TestController@sendsms");
Route::any('/test/savecode',"TestController@savecode");
Route::any('/test/code',"TestController@tocode");
Route::any('/getip',"TestController@getip");
Route::any('/test/import',"TestController@import");
Route::any('/test/shai',"TestController@shai");
Route::any('/test/export',"TestController@export");
//
Auth::routes();

Route::get('/feng/home', 'HomeController@index');
Route::any('/update/{id}', 'HomeController@update');
Route::get('/home/list', 'HomeController@lists');
Route::get('/select/{id}', 'HomeController@select');
//
//Route::group(['middleware' => ['web']], function () {
////用户认证
//    Route::get('/login', 'Auth\AuthController@getLogin');
//    Route::post('/login', 'Auth\AuthController@postLogin');
//    Route::get('/logout', 'Auth\AuthController@getLogout');
////后台退出登录
//    Route::get('/auth/logout', function () {
//        session()->flush();
//        return redirect('/login');
//    });
//});
