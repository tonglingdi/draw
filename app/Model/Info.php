<?php
/**
 * Created by PhpStorm.
 * User: we-te
 * Date: 2017/3/6
 * Time: 13:47
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

Class Info extends Model
{
    /**
     * @param $id
     * @return bool
     * 根据用户是否已经留过信息  判断用户第几次转
     */
    public static function check($id)
    {
        $info = Info::where(array('user_id' => $id))->count();
        if ($info) {
            return '2';
        } else {
            return '1';
        }
    }

}