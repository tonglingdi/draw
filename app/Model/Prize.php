<?php
/**
 * Created by PhpStorm.
 * User: we-te
 * Date: 2017/3/6
 * Time: 13:47
 */

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

Class Prize extends Model
{
    protected  $table='prize';

    const code=array(
        '1'=>'num_you',
        '2'=>'num_xiaomi',
        '3'=>'num_e',
        '4'=>'num_ipad',
        '5'=>'num_tent'

    );

    /**
     * @param $data
     * @return bool
     * 判断当前奖品赠送数量 是否超出限制
     */
    public static function check($data)
    {
        if($data == '6'){
            return true;
        }else{
            $info = Prize::where('id', $data)->first();
    //        $count = Record::where(array('pid' => $data, 'win' => '1','date'=>date("Y-m-d")))->where("code","!=","0")->count();
            if($data=='1' || $data=='3'){
                $count = Redis::get(self::code[$data]);
                if ($count >= $info->value || $info->stock <= 0) {
                    return true;
                } else {
                    return false;
                }
            }else{
                $count = Record::where(array('pid' => $data, 'win' => '1','date'=>date("Y-m-d")))->count();
                if ($count >= $info->value) {
                    return true;
                } else {
                    return false;
                }
            }
        }

    }

    /**
     * @param $id
     * @return bool
     * 判断用户是否中奖
     */
    public static  function  win($id){
        $count = Record::where(array('user_id' => $id, 'win' => '1'))->first();
        if(!empty($count)){
            return $count->pid;
        }else{
            return '';
        }
    }

}