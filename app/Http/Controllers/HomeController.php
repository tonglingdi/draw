<?php

namespace App\Http\Controllers;

use App\Model\Info;
use App\Model\Prize;
use App\Model\Record;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Prize::get();
        return view('admin.data',['list'=>$list]);
    }

    public function  update(Request $request,$id){
        if($request->isMethod("post")){
            $data=array(
                'value'=>$request->value,
                'proab'=>$request->proab
            );

            Prize::where(['id'=>$request->id])->update($data);
            return redirect("/feng/home");


        }else{
            $list = Prize::find($id);
            return view('admin.update',['data'=>$list]);

        }

    }

    public function lists(Request $request){
        $arr=array();
        if($request->phone){
            $arr["phone"]=$request->phone;
        }
       $list =  Info::where($arr)->orderby("id","desc")->paginate(20);
        return view('admin.list',['list'=>$list]);
    }

    public function select($userid){

       $data = Record::where("user_id",$userid)->get();

        return view("admin.record",['data'=>$data]);

    }
}
