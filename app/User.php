<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * 判断用户是否有抽奖权限
     */

    public static function check($id)
    {
//        return true;
        $info = User::find($id);
        if ($info->win_num > 0 || $info->nums <=0) {
            return false;
        } else {
            return true;
        }
    }


    public static  function  updates($where= "",$data=""){
        return User::where($where)->update($data);
    }
    public static  function  increments($where,$key ,$data = '1'){
        return User::where($where)->increment($key,$data);
    }
    public static  function  decrements($where,$key ,$data = '1'){
        return User::where($where)->decrement($key,$data);
    }


}
