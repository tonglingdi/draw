<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>广汽丰田丰云惠</title>
    <meta content="width=device-width, initial-scale=1.0,maximum-scale=1.0,user-scalable=0" name="viewport">
    <link rel="stylesheet" href="/css/style.css?v=1">
    <script src="https://cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
    <script src="http://res.wx.qq.com/open/js/jweixin-1.1.0.js" type="text/javascript"></script>
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?715123c9d5b7ef86f0908f1407b072a4";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <script type="text/javascript" >
        $.ajax({
            type:"get",
            url:'http://jssdk.tonglingdi.cn/getjssdk',
            async:false,
            data: {url: document.URL},
            dataType: "json",
            success:function (data) {
                signPackage = data;
                wx.config({
                    debug: false,
                    appId: signPackage.appId,
                    timestamp: signPackage.timestamp,
                    nonceStr: signPackage.nonceStr,
                    signature: signPackage.signature,
                    jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage']
                });
                wx.ready(function() {
                    wx.onMenuShareTimeline({
                        title:"完全停不下来的幸运转盘，快来试手气", // 分享标题
                        link: "http://"+window.location.host + "/", // 分享链接
                        imgUrl: "http://"+window.location.host + "/images/share.jpg", // 分享图标
                        success: function() {
                        },
                        cancel: function() {
                            // 用户取消分享后执行的回调函数
                        }
                    });
                    wx.onMenuShareAppMessage({
                        title: '完全停不下来的幸运转盘，快来试手气', // 分享标题
                        desc: "各种惊喜随你转", // 分享描述
                        link: "http://"+window.location.host + "/", // 分享链接
                        imgUrl: "http://"+window.location.host + "/images/share.jpg", // 分享图标
                        success: function() {
                        },

                        cancel: function() {
                            // 用户取消分享后执行的回调函数
                        }
                    });
                });
            },
            error:function (data) {

            }
        });
    </script>
</head>
<body>
<script>!(function(a,b,c,d,e,f){var g="",h=a.sessionStorage,i="__admaster_ta_param__",j="tmDataLayer"!==d?"&dl="+d:"";
        if(a[f]={},a[d]=a[d]||[],a[d].push({startTime:+new Date,event:"tm.js"}),h){var k=a.location.search,
                l=new RegExp("[?&]"+i+"=(.*?)(&|#|$)").exec(k)||[];l[1]&&h.setItem(i,l[1]),l=h.getItem(i),
        l&&(g="&p="+l+"&t="+ +new Date)}var m=b.createElement(c),n=b.getElementsByTagName(c)[0];m.src="//tag.cdnmaster.cn/tmjs/tm.js?id="+e+j+g,
                m.async=!0,n.parentNode.insertBefore(m,n)})(window,document,"script","tmDataLayer","TM-725256","admaster_tm");</script>
@yield('content')
</body>
</html>