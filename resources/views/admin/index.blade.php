@extends("layouts.app")
@section('content')
    <link rel="stylesheet" href="{{url("/bower_components/AdminLTE/plugins/iCheck/all.css")}}">
    <section class="content-header">
        <h1>
            基本设置
            <small>Add Basic</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
            {{--<li><a href="javascript:void(0);">Wechat</a></li>--}}
            <li class="active">basic</li>
        </ol>
    </section>
    <div class="row">
        <div class="col-sm-12">
            @include("layout.message")
        </div>
    </div>
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Basic Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post">
                {{csrf_field()}}
                <div class="box-body" id="container">
                    @foreach($basic as $value)
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-2 control-label">{{ $value->desc }}
                                [{{ $value->name }}]:</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="{{$value->name}}" required
                                       name="{{$value->name}}"
                                       value="{{$value->value}}"
                                       placeholder="{{ $value->desc }}">
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-success">提交</button>
                    <button type="button" class="btn btn-default pull-right btn-addBasic" data-toggle="modal"
                            data-target="#addBasic">增加
                    </button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
    </section>
    @include("layout.addBasic")
    <script src="{{url("/static/King/js/add_link.js")}}"></script>
@endsection
