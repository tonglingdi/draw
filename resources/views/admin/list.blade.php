@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="{{url("/bower_components/AdminLTE/plugins/iCheck/all.css")}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{url("/bower_components/AdminLTE/plugins/datepicker/datepicker3.css")}}">
    <section class="content-header">
        <h1>
            信息列表
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">搜索</h3>
                        <div class="box-tools"></div>
                    </div>
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <form class="form-inline" role="form" method="get" id="frmSearch">
                                <div class="form-group">
                                    <input type="text" name="phone" class="form-control input-sm" style="width:15em" >
                                </div>&nbsp;
                                <input type="submit" class="btn  btn-success" value="搜索">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-header with-border box-danger">
                        <h3 class="box-title">信息列表</h3>
                        <div class="box-tools">
                            <div class="form-inline pull-right">

                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example2" class="table table-bordered table-hover dataTable" role="grid"
                                           aria-describedby="example2_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-sort="ascending"
                                                aria-label="Rendering engine: activate to sort column descending">
                                                编号
                                            </th>
                                            <th class="sorting" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Browser: activate to sort column ascending">
                                                姓名
                                            </th>
                                            <th class="sorting" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                手机号
                                            </th>
                                            <th class="sorting" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                省份
                                            </th>
                                            <th class="sorting" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                城市
                                            </th>
                                            <th class="sorting" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                门店
                                            </th>
                                            <th class="sorting" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                操作
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($list as $value)
                                            <tr role="row" class="odd">
                                                <td class="sorting_1">{{$value->id}}</td>
                                                <td>{{$value->name}}</td>
                                                <td>{{$value->phone}}</td>
                                                <td>{{$value->province}}</td>
                                                <td>{{$value->city}}</td>
                                                <td>{{$value->shop}}</td>
                                                <td>
                                                    <a href="/select/{{$value->user_id}}">查看</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
