@extends("layouts.app")
@section('content')
    <section class="content-header">
        <h1>
            抽奖详情
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box ">

                    @foreach($data as $val)
                        <div class="box-body">
                            <div class="form-group">
                                <label>时间:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-sticky-note-o"></i>
                                    </div>
                                    <input type="text" name="value" class="form-control" required
                                           value="{{$val->created_at}}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>是否中奖:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-sticky-note-o"></i>
                                    </div>
                                    <input type="text" name="value" class="form-control" required
                                           value="{{$val->win=='1'?'中奖':"未中奖"}}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>核销码:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-sticky-note-o"></i>
                                    </div>
                                    <input type="text" name="proab" class="form-control" required
                                           value="{{$val->code}}"/>
                                </div>
                            </div>

                        </div>
                        @endforeach
                </div>

            </div>
        </div>
    </section>
@endsection

