@extends("layouts.app")
@section('content')
    <section class="content-header">
        <h1>
            修改活动
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box ">
                    <form action="" method="post">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label>每天领取的数量:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-sticky-note-o"></i>
                                    </div>
                                    <input type="text" name="value" class="form-control" required
                                           value="{{$data->value}}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>概率:</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-sticky-note-o"></i>
                                    </div>
                                    <input type="text" name="proab" class="form-control" required
                                           value="{{$data->proab}}"/>
                                </div>
                            </div>

                            <input type="hidden" name="id" value="{{$data->id}}">

                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-flat">修改</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>
    <script src="{{url("/static/King/js/add_link.js")}}"></script>
    <script type="text/javascript">
        $("input[type=radio]").each(function () {
            if ($(this).data('num') == $("input[name=num]").val()) {
                $(this)[0].checked = true;
            }
        })
        $("input[type=radio]").on("change", function () {
            $("input[name=num]").val($(this).data('num'))
        })
    </script>
@endsection

