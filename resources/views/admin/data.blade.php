@extends('layouts.app')
@section('content')
    <link rel="stylesheet" href="{{url("/bower_components/AdminLTE/plugins/iCheck/all.css")}}">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{url("/bower_components/AdminLTE/plugins/datepicker/datepicker3.css")}}">
    <section class="content-header">
        <h1>
            活动设置
        </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-header with-border box-danger">
                        <h3 class="box-title">活动列表</h3>
                        <div class="box-tools">
                            <div class="form-inline pull-right">

                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example2" class="table table-bordered table-hover dataTable" role="grid"
                                           aria-describedby="example2_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-sort="ascending"
                                                aria-label="Rendering engine: activate to sort column descending">
                                                编号
                                            </th>
                                            <th class="sorting" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Browser: activate to sort column ascending">
                                                奖品
                                            </th>
                                            <th class="sorting" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                概率
                                            </th>
                                            <th class="sorting" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                每天获取数量
                                            </th>
                                            <th class="sorting" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="Platform(s): activate to sort column ascending">
                                                类型
                                            </th>
                                            <th class="sorting" aria-controls="example2" rowspan="1"
                                                colspan="1" aria-label="CSS grade: activate to sort column ascending">
                                                操作
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($list as $value)
                                            <tr role="row" class="odd">
                                                <td class="sorting_1">{{$value->id}}</td>
                                                <td>{{$value->prize}}</td>
                                                <td>{{$value->proab}}</td>
                                                <td>{{$value->value}}</td>
                                                <td>{{$value->type}}</td>
                                                <td>
                                                    <a href="/update/{{$value->id}}">修改</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
