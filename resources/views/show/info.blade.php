@extends('layouts.common')
@section('content')
    <script src="/js/address.js?v=1"></script>

    <style>
        body, html .demo, p {
            padding: 0;
            margin: 0;
            overflow: hidden;
            font-size: 16px;
        }

        a, button, input, img {
            outline: none;

            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);

            -webkit-tap-highlight-color: transparent

        }

        .main {
            overflow: hidden;
        }

        .main {
            background: url('/images/info.jpg') 100% 100%;
            width: 100%;
            height: 100%;
            background-size: 100% 100%;
            position: relative;
        }

    </style>
    </head>
    <div class=main>
        @if($nums=='')
            <div class="title">
                <p>填写个人信息，再去转一次</p>
            </div>
        @else
            <div class="title">
                <p>填写个人信息，获取到店核销码</p>
            </div>
        @endif

        <form id="form" action="" method="post">
            <div class="name info-div">
                <label style="" class="label"><img src="/images/ic-name.jpg"></label>
                <input type="text" name="name" placeholder="姓名">
            </div>
            <div class="phone info-div">
                <label style="" class="label"><img src="/images/ic-phone.jpg"></label>
                <input type="text" name="phone" placeholder="电话">
            </div>
            <div class="area">
                <div class="province">
                    <label style="" class="label"><img src="/images/ic-province.jpg" style="width: 30%"></label>
                    <select name="province" class="provincelist">
                        <option selected value="">省份</option>
                        @foreach($province as $val)
                            <option value="{{$val->province_code}}" data-code="{{$val->province_code}}">{{$val->province_name}}</option>
                        @endforeach
                    </select>
                </div>


                <div class="city">
                    <label style="" class="label"><img src="/images/ic-city.png" style="width: 30%"></label>
                    <select name="city" class="citylist">
                        <option selected value="">城市</option>
                    </select>
                </div>
            </div>
            <div class="position info-div">
                <label style="" class="label"><img src="/images/ic-shop.jpg"></label>
                <select name="shop" class="shop">
                    <option selected value="">意向销售店</option>
                </select>
            </div>
            <div class="type info-div">
                <label style="" class="label"><img src="/images/ic-type.jpg"></label>
                <select name="type">
                    @if($nums =='1')
                        <option value="004014">雷凌</option>
                     @else
                        <option selected value="">意向车型</option>
                        @foreach($modles as $key => $val)
                            <option value="{{$key}}">{{$val}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="time info-div">
                <label style="" class="label"><img src="/images/ic-time.jpg"></label>
                <select name="time">
                    <option selected value="">购车时间</option>
                    @foreach($time as $val)
                        <option value="{{$val}}">{{$val}}</option>
                    @endforeach
                </select>
            </div>
            <div class="submit" ></div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="nums" value="{{ $nums}}">
        </form>

        <div class="info-error">
            <img src="/images/error.png"/>
            <p></p>
            <div class="in-close"></div>
        </div>
    </div>

    <div class="info-alert" style="
position: fixed;
    height: 100%;
    width: 100%;
    top: 0;
    z-index: 3;
    display:none;
    background: #000;
    opacity: 0.6;">
    </div>


    <script>
        var province = JSON.parse(province);
        var city = JSON.parse(city);
        function trim(str){ //删除左右两端的空格
            return str.replace(/(^\s*)|(\s*$)/g, "");
        }
        function show_error(message) {
            $('.info-error p').text(message);
            $('.info-error,.info-alert').show();
        }
        $(".in-close").click(function () {
            $('.info-error,.info-alert').hide();
        });
        $(function () {

            $(".provincelist").change(function () {
//                var code = $(this).val();
                var code = $(".provincelist option:selected").data("code");
//                console.log(code);

                var data = province[code];

                $(".citylist option[value!='']").hide();
                var str = '';
                $.each(data, function (index, value) {
                    str += '<option value="' + value.city_code + '" data-code = "'+ value.city_code  +'">' + value.city_name + '</option>';
                });
                $(".citylist").append(str);

            });
            /**
             * 获取门店列表
             */
            $(".citylist").change(function () {
                var code = $(".citylist option:selected").data("code");
                var data = city[code];
                $(".shop option[value!='']").hide();
                var str = '';
                $.each(data, function (index, value) {
                    str += '<option value="' + value.dealer_code + '">' + value.dealer_name + '</option>';
                });
                $(".shop").append(str);
            });


        $(".submit").click(function () {
            var name = trim($('input[name=name]').val());
            var phone = trim($('input[name=phone]').val());
            var province = $('select[name=province]').val();
            var city = $('select[name=city]').val();
            var shop = $('select[name=shop]').val();
            var type = $('select[name=type]').val();
            var time = $('select[name=time]').val();
            _smq.push(['custom', 'm_mall_information', 'm_mall_information_lead_m_mall_information_submit', phone]);
            if (name == '') {
                show_error("请输入姓名");
                return;
            }
            if (phone == '') {
                show_error("请输入手机号");
                return;
            }
            if (phone.length != 11 || !phone.match(/^1[3|5|7|8][0-9]{9}$/)) {
                show_error("请输入正确的手机号");
                return;
            }
            if (province == '') {
                show_error("请选中省份");
                return;
            }
            if (city == '') {
                show_error("请选择城市");
                return;
            }
            if (shop == '') {
                show_error("请选择意向销售店");
                return;
            }
            if (type == '') {
                show_error("请选择意向车型");
                return;
            }
            if (time == '') {
                show_error("请输入意向购车时间");
                return;
            }
            $.ajax({
                url: '/check_tel/' ,
                async: false,
                type: "GET",
                data: {phone:phone},
                dataType: "json",
                error: function () {

                },
                success: function (data) {
                    if(data.code=='1'){
                        if (name != '' && phone != '' && province != '' && city != '' && shop != '' && type != '' && time != '') {
                            $("#form").submit();
                        }
                    }else{
                        show_error("手机号已经被注册");
                    }

                }
            });

        });
        });
    </script>
@endsection