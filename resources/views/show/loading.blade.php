<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>填写信息</title>
    <meta content="width=device-width, initial-scale=1.0,maximum-scale=1.0,user-scalable=0" name="viewport">
    <script type="text/javascript" src="/js/jquery-1.8.3.js"></script>
</head>
<style type="text/css">
    body, html .demo, p {
        padding: 0;
        margin: 0;
        overflow: hidden;
        font-size: 16px;
    }

    .loading {
        width: 100%;
        height: 100%;
        position: relative;
    }
    .load{
        position: absolute;
        top: 40%;
        width: 100%;
        z-index: 5;
        left: 0%;
    }
    .load img{
        width: 100%;
    }
    .red{
        position: absolute;
        top: 43%;
        width: 35%;
        left: 0%;
        background-color: red;
        height: 60px;
        z-index: 3;
    }
    .ready{
        position: absolute;
        top: 53%;
        z-index: 6;
        width: 100%;
        text-align: center;
        font-weight: bold;
        font-size: 0.9rem;
    }

</style>
<body>

<div class="loading" style="background-color: #f2bd15 ">
    <div class="red "></div>
    <div class="yellow load"><img src="/images/load.png" class="y-img"></div>
    <div class="ready">正在准备礼品……</div>
</div>
<script type="text/javascript">
    $(function () {
//        alert($(window).width());
        alert($(window).height());
        $(".toshare").click(function () {
            $(".share").show();
        });
        for(var i=0;i<=100;i++){
            console.log(i);
            if(i%3==0){
                var left=i/3+'%';
                console.log(left);
                $(".red").css("left",left)
            }
            if(i==100){
                $(".red").css("left",'35%')
            }
        }
        $(".share").click(function () {
            $(".share").hide();
        });
    });
</script>

</body>
</html>