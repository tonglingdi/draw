@extends('layouts.common')
@section('content')
<style>
    .sel-intro,.close {
        outline: none;

        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);

        -webkit-tap-highlight-color: transparent

    }
</style>
<div class="loading" style="background-color: #f2bd15 ">
    <div class="red " style="display: none"></div>
    <div class="yellow load"><img src="/images/load.png" class="y-img"></div>
    {{--<div class="ready">正在准备礼品……</div>--}}
</div>
{{--<audio preload="auto" src="https://hdg.faisys.com/image/zhuanpan/bgmusic00M.mp3" loop=""></audio>--}}
<audio class='voice' id='voice' src="/music/music.mp3" preload='auto' autostart="false"  style="display: none;overflow: hidden;position: absolute;text-align: center;top: 0%;" >
</audio>
<audio class='music' id='music' src="/music/6125.mp3" preload='auto' autostart="false" style=" display: none;overflow: hidden;position: absolute;text-align: center;top: 0%;" >
</audio>

<div class="demo">
    <img src="/images/disk.jpg" id="disk"/>
    <img src="/images/hand.png" class="hand" />
    <div class="table">
        <img src="/images/turn.png" id="turn" class="turn" />
        <div id="start" class="start">
            <img id="startbtn" src="/images/start.png" style="cursor: pointer; transform: rotate(0deg);">
            <p>GO</p>
        </div>
    </div>

    <div class="sel-intro"></div>
    <div class="intro" style="display: none">
        <img src="/images/intro.png"/>
        <div class="close"></div>
    </div>
    @if(!empty($list->all()))
        <ul class="p-list" data-num="{{isset($list[0]->rid)?$list[0]->rid:0}}">
            @foreach($list as $val)
                <li>>恭喜{{substr_replace($val->phone,"****",3,4)}}获得{{$val->prize}}</li>
            @endforeach
        </ul>
    @endif
    <div class="alert">
        <div class="success" style="display: none">
            {{--<div class="al-img"><img src="/images/alert.png"/></div>--}}
            <p class="tishi"></p>
            <p class="content"></p>
            <a href="" class="al-href" onclick="_smq.push(['custom', 'm_mall_ZP_award', 'm_mall_ZP_award_check-present']);"></a>
        </div>
        <div class="error" style="display: none">
            <div class="al-img"><img src="/images/noget.png"/></div>
            <a href="/write/info?nums=1" onclick="_smq.push(['custom', 'm_mall_ZP_award', 'm_mall_ZP_award_info-for-chance']);" class="w-info"></a>
        </div>
        <div class="error2" style="display: none">
            <div class="al-img"><img src=""/></div>
            <a class="d-close"></a>
        </div>
    </div>
</div>


<script type="text/javascript" src="/js/jQueryRotate.2.2.js"></script>

<script type="text/javascript">
    function is_weixin(){
        var ua = navigator.userAgent.toLowerCase();
        if(ua.match(/MicroMessenger/i)=="micromessenger") {
//            $(".al-href").css("top",'64.5%');
            var twidth= $(window).width() * 0.8;//转盘的宽度
            var swidth =$(window).width() * 0.2;
            var width =twidth/2 - swidth/2;
           var pheight = $("#start p").height();
            $("#start").css("top",width + 'px');
            $("#start p").css("top",swidth/2 -pheight/2 +'px')
        } else {
//            $(".al-href").css("top",'67%');
            var twidth= $(window).width() * 0.7;//转盘的宽度
            var swidth =$(window).width() * 0.2 ;
            var width =twidth/2 - swidth/2 -10;
            var pheight = $("#start p").height();
            var height = $("#start").height();
            $("#start").css("top",width + 'px');
            console.log(height/2,pheight/2);
            $("#start p").css("top",height/2 -pheight/2 +'px')
            $(".table").addClass("table1");
            $(".turn").addClass("turn1")
            $(".start").addClass("start1")
        }

        var alheight = $(window).width() * 453/640;
        $(".tishi").css("top",alheight *0.2562 +'px');
        $(".content").css("top",alheight *0.4193 +'px');
        $(".al-href").css("top",alheight *0.748 +'px');

    }
    var audioAutoPlay = function(id) {
        var audio = document.getElementById(id),
                play = function() {
                    audio.play();
                };
        audio.play();
    };
    var status = "{{$status}}";
    var storage = window.localStorage;
//    document.addEventListener("WeixinJSBridgeReady", function () {
////        $(".voice")[0].play();
//    }, false);
//    document.addEventListener('YixinJSBridgeReady', function () {
////        $(".voice")[0].play();
//    }, false);
    function show(data) {
        $(".tishi").text(data['tishi']);
        $(".content").text(data['content']);
        $(".al-href").text(data['al-href']);
        $(".al-href").attr("href", data['href']);
        $(".success").show();
    }
    function error2(src) {
        $(".error2 img").attr('src',src);
        $(".error2").show();
    }
    $(function () {

//        var iwidth= $(".turn").height();
//        console.log(iwidth);

        var imgPath = '/images/';
        var loadImage = function (path, callback) {
            var img = new Image();
            img.onload = function () {
                img.onload = null;
                callback(path)
            };
            img.src = path
        };
        var loadingPage = function () {
            var sourceArr = ["alert.png", "disk.jpg", "intro.png", 'noget.png', "noget2.png", 'start.png','nonum.png'];
            $(".red").show();
            for (var i = 0; i < sourceArr.length; i++) {
                sourceArr[i] = (imgPath + sourceArr[i])
            }
            var imgLoader = function (imgs, callback) {
                var len = imgs.length,
                        i = 0;
                while (imgs.length) {
                    loadImage(imgs.shift(), function (path) {
                        callback(path, ++i, len)
                    })
                }
            };
            imgLoader(sourceArr, function (path, curNum, total) {
                var percent = curNum / total;
                if (percent < 0.143) {
                    $(".red").css("left", '5%')
                }
                if (percent < 0.29 && percent > 0.143) {
                    $(".red").css("left", "10%")
                }
                if (percent < 0.43 && percent > 0.29) {
                    $(".red").css("left", "15%")
                }
                if (percent < 0.58 && percent > 0.43) {
                    $(".red").css("left", '20%')
                }
                if (percent < 0.72 && percent > 0.58) {
                    $(".red").css("left", '25%')
                }
                if (percent < 0.86 && percent > 0.72) {
                    $(".red").css("left", '30%')
                }
                if (percent >= 1) {
                    $(".red").css("left", '34%')
                    setTimeout(function () {
                        $(".loading").hide();
                        $(".demo").show();
                          is_weixin();
                    }, 500);

//
                }
            })
        };
        loadingPage();



        $("#startbtn").click(function () {
            _smq.push(['custom', 'm_mall_ZP', 'm_mall_ZP_GO']);
//            audioAutoPlay("voice");
            lottery();
        });
        $(".sel-intro").click(function () {
            _smq.push(['custom', 'm_mall_ZP', 'm_mall_ZP_rule']);
            $(".intro").show();
        });
        $(".close").click(function () {
            _smq.push(['custom', 'm_mall_ZP', 'm_mall_ZP_rule_close']);
            $(".intro").hide();
        });
        $(".d-close").click(function () {
            var src=$(".error2 img").attr("src");
            if(src='/images/nonum.png'){
                _smq.push(['custom', 'm_mall_ZP_award', 'm_mall_ZP_award_no-chance_close']);
            }else{
                _smq.push(['custom', 'm_mall_ZP_award', 'm_mall_ZP_award_no-present_close']);
            }
            $(".error2").hide();
        });
//        setInterval('autoScroll(".p-list")',2000);
        setInterval('scroll_news()', 3000);
//        setInterval('getdata()', 2000);
    });


    function getdata() {
        var id = $(".p-list").data("num");
        if (id == undefined) {
            id = 0;
        }
        $.ajax({
            type: 'get',
            url: '/prize/' + id,
            dataType: 'json',
            cache: false,
            error: function () {
                alert('出错了！');
                return false;
            },
            success: function (json) {
                var length = json.length;
                if (length == '0') {
                } else {
                    $(".p-list").data("num", Number(id) + Number(length));
                    for (var i = 0; i < length; i++) {
                        var phone = json[i].phone.substr(0, 3) + '****' + json[i].phone.substr(7);
                        var str = '<li>恭喜' + phone + '获得' + json[i].prize + '</li>';
                        $(".p-list li").eq(0).after(str);
                    }
                }

            }
        });
    }
    function autoScroll(obj) {
        $(obj).animate(function () {
            $(this).css({marginTop: "0px"}).find("li:first").appendTo(this);
        }, 500, function () {
            $(this).css({marginTop: "0px"}).find("li:first").appendTo(this);
        })
    }
    function scroll_news() {
        $(function () {
            $('.p-list li').eq(0).fadeOut('slow', function () {
                //   alert($(this).clone().html());
                //克隆:不用克隆的话，remove()就没了。
                $(this).clone().appendTo($(this).parent()).fadeIn('slow');
                $(this).remove();
            });
        });
    }
    function lottery() {

        if (status) {
            audioAutoPlay("voice");
            $.ajax({
                type: 'get',
                url: '/start',
                dataType: 'json',
                cache: false,
                error: function () {
                    alert('出错了！');
                    return false;
                },
                success: function (json) {
                    $("#startbtn").unbind('click').css("cursor", "default");
                    if (json.err == '0') {
                        error2("/images/nonum.png");
                    } else {

                        var a = json.angle; //角度;
                        var p = json.type; //奖项
                        var pid = json.id; //奖项
                        $("#turn").rotate({
                            duration: 6500, //转动时间
                            angle: 0,
                            animateTo: 1800 + a, //转动角度
                            easing: $.easing.easeOutSine,
                            callback: function () {
                                if (pid > 5) {
                                    //判断是第几次没抽中  第一次去填写信息页面
                                    var a = storage.a ? storage.a : 0;
                                    storage.a = Number(a) + 1;
                                    if (storage.a == '1') {//第一次没有中
                                        $(".error").show();
                                    } else {
                                        error2("/images/noget2.png");
                                    }
                                } else {
                                    audioAutoPlay("music");
                                    var data = new Array();
                                    data['tishi'] = '人品爆发';
                                    data['content'] = '获得' + p;
                                    data['href'] = '/detail';
                                    data['al-href'] = '查看礼品 >>';
                                    show(data);
//                                    }
                                }
                            }
                        });
                    }
                }
            });
        } else {
            error2("/images/nonum.png");
        }
    }
</script>

@endsection