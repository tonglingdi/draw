@extends('layouts.common')
@section('content')
<style type="text/css">
    body, html .demo, p {
        padding: 0;
        margin: 0;
        /*overflow: hidden;*/
        font-size: 16px;
    }

    .main {
        width: 100%;
        height: 100%;
        position: relative;
        /*overflow: hidden;*/
    }

    .code {
        width: 100%;
        height: 100%;
    }

    .code img {
        width: 100%;
    }

    .toshare {
        position: absolute;
        bottom: 3%;
        width: 60%;
        left: 20%;
        height: 85px;
    }
    .share{
        display: none;
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        bottom: 0;
        background: rgba(0,0,0,0.5);
    }
    .share img{
        position: absolute;
        right: 3%;
        WIDTH: 25%;
    }
</style>
<body>
<div class="main">
    <div class="code">

        <img src="/images/getcode.jpg"/>
    </div>
    <div class="toshare"></div>
    <div class="share">
        <img src="/images/share.png"/>
    </div>
</div>
<script type="text/javascript">
    $(function () {
            var ua = navigator.userAgent.toLowerCase();
            if(ua.match(/MicroMessenger/i)=="micromessenger") {

            } else {
                $(".toshare").hide();
            }

        var height = $(window).width() * 1038 / 640;
        $(".toshare").css("top",0.796*height)
        $(".toshare").click(function () {
            _smq.push(['custom', 'm_mall_ZP_code', 'm_mall_ZP_code_share']);
            $(".share").show();
        });
        $(".share").click(function () {
            $(".share").hide();
        });
    });
</script>
@endsection