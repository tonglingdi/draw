<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>查看礼品</title>
    <meta content="width=device-width, initial-scale=1.0,maximum-scale=1.0,user-scalable=0" name="viewport">
    <link rel="stylesheet" href="/css/style.css">
</head>
<style type="text/css">
    body, html .demo, p {
        padding: 0;
        margin: 0;
        overflow: hidden;
        font-size: 16px;
    }

    .main {
        width: 100%;
        height: 100%;
        position: relative;
    }

</style>
<body>
<div class="main">
    {{--<img src="/images/prize.jpg" class="prize"/>--}}
    {{--<div class="p-content">--}}
    {{--<p class="p-title">获得价值{{$prize["name"]}}</p>--}}
    {{--<div class="p-photo @if($weixin) p-photo{{$id}} @else l-photo{{$id}}   @endif"><img src="/images/{{$prize["path"]}}" class="pimg{{$id}}"/></div>--}}
    {{--</div>--}}
    @if($num=='1')
        <img src="/images/d-{{$num}}-{{$id}}.jpg" class="prize"/>
        <a href="/write/info" class="p-sub">留下资料去领取 >>></a>
    @else
        <img src="/images/d-2-{{$id}}.jpg" class="prize"/>
        <a href="/getcode" class="p-sub">获取奖品核销码 >>></a>
    @endif
    {{--<table class="p-way">--}}
    {{--<tr>--}}
    {{--<td>兑换方式:</td>--}}
    {{--<td>--}}
    {{--{{$prize["way"]}}--}}
    {{--</td>--}}
    {{--</tr>--}}
    {{--</table>--}}

</div>
{{--奖品名称:{{$prize["name"]}}<br>--}}
{{--兑换方式:{{$prize["way"]}}--}}
</body>
</html>