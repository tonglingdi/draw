<html>
<head>
    <meta charset='utf8'>
    <meta content="width=device-width, initial-scale=1.0,maximum-scale=1.0,user-scalable=0" name="viewport">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <script src="https://cdn.bootcss.com/jquery/2.2.4/jquery.min.js"></script>
    <link rel="stylesheet" href="/css/style.css">

    <style>
        body, html .demo, p {
            padding: 0;
            margin: 0;
            overflow: hidden;
            font-size: 16px;
        }

        a, button, input, img {
            outline: none;

            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);

            -webkit-tap-highlight-color: transparent

        }

    </style>
</head>
<body>
<div class=main>
    @if($nums!=='')
        <div class="title">
            <p>填写个人信息，再去转一次</p>
        </div>
    @else
        <div class="title">
            <p>填写个人信息，获取到店核销码</p>
        </div>
    @endif

    <form id="form" action="" method="post">
        <div class="name">
            <input type="text" name="name" placeholder="姓名">
        </div>
        <div class="phone">
            <input type="text" name="phone" placeholder="电话">
        </div>
        <div class="area">
            <div class="province">
                <select name="province">
                    <option selected value="">省份</option>
                    <option value="1">省份</option>
                    <option value="2">省份</option>
                    <option value="3">省份</option>
                    <option value="4">省份</option>
                </select>
            </div>


            <div class="city">
                <select name="city">
                    <option selected value="">城市</option>
                    <option value="1">省份</option>
                    <option value="2">省份</option>
                    <option value="3">省份</option>
                    <option value="4">省份</option>
                </select>
            </div>
        </div>
        <div class="position">
            <select name="shop">
                <option selected value="">意向销售店</option>
                <option value="1">省份</option>
                <option value="2">省份</option>
                <option value="3">省份</option>
                <option value="4">省份</option>
            </select>
        </div>
        <div class="type">
            <select name="type">
                <option selected value="">意向车型</option>
                <option value="1">省份</option>
                <option value="2">省份</option>
                <option value="3">省份</option>
                <option value="4">省份</option>
            </select>
        </div>
        <div class="time">
            <select name="time">
                <option selected value="">购车时间</option>
                <option value="1">省份</option>
                <option value="2">省份</option>
                <option value="3">省份</option>
                <option value="4">省份</option>
            </select>
        </div>
        <div class="submit"></div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="nums" value="{{ $nums}}">
    </form>
    <div class="info-error">
        <img src="/images/error.png"/>
        <p></p>
        <div class="in-close"></div>
    </div>
</div>


<script>

    function show_error(message) {
        $('.info-error p').text(message);
        $('.info-error').show();
    }
    $(".in-close").click(function () {
        $('.info-error').hide();
    });

    $(".submit").click(function () {
        var name = $('input[name=name]').val();
        var phone = $('input[name=phone]').val();
        var province = $('select[name=province]').val();
        var city = $('select[name=city]').val();
        var shop = $('select[name=shop]').val();
        var type = $('select[name=type]').val();
        var time = $('select[name=time]').val();
        if (name == '') {
            show_error("请输入姓名");
            return;
        }
        if (phone == '') {
            show_error("请输入手机号");
            return;
        }
        if (phone.length != 11 || !phone.match(/^1[3|5|7|8][0-9]{9}$/)) {
            show_error("请输入正确的手机号");
            return;
        }
        if (province == '') {
            show_error("请选中省份");
            return;
        }
        if (city == '') {
            show_error("请选择城市");
            return;
        }
        if (shop == '') {
            show_error("请选择意向销售店");
            return;
        }
        if (type == '') {
            show_error("请选择意向车型");
            return;
        }
        if (time == '') {
            show_error("请输入意向购车时间");
            return;
        }
        if (name != '' && phone != '' && province != '' && city != '' && shop != '' && type != '' && time != '') {
            $("#form").submit();
        }
    });
</script>
</body>
</html>