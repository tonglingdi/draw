@extends('layouts.common')
@section('content')
    <style type="text/css">
        body, html .demo, p {
            padding: 0;
            margin: 0;
            overflow: hidden;
            font-size: 16px;
        }

        .main {
            width: 100%;
            height: 100%;
            position: relative;
        }

    </style>
    <div class="main">
        <img src="/images/prize.jpg" class="prize"/>
        <div class="p-content">
            <p class="p-title">获得价值{{$prize["name"]}}</p>
            <div class="p-photo @if($weixin) p-photo{{$id}} @else l-photo{{$id}}   @endif"><img
                        src="/images/{{$prize["path"]}}" class="pimg{{$id}}"/></div>
        </div>
        @if($code != '1')
            @if($num=='1')
                <a href="/write/info" class="p-sub" onclick="_smq.push(['custom', 'm_mall_ZP_present', 'm_mall_ZP_present_info-for-present']);" >留下资料去领取 >>></a>
            @else
                <a href="/getcode" onclick="_smq.push(['custom', 'm_mall_ZP_present', 'm_mall_ZP_present_get-code']);" class="p-sub">获取奖品核销码 >>></a>
            @endif
        @endif
        <table class="p-way">
            <tr>
                <td>兑换方式:</td>
                <td>
                    {{$prize["way"]}}
                </td>
            </tr>
        </table>

    </div>
@endsection