@extends('layouts.show')
@section('content')
    <audio class='voice' id='voice' src="https://hdg.faisys.com/image/zhuanpan/bgmusic00M.mp3" preload='auto' autoplay style="display: none;overflow: hidden;position: absolute;text-align: center;top: 0%;" > </audio>

    <div class="pc-main" >

        <img src="/image/index.jpg" class="index-img" id="head"/>
        <div class="trun">
            <img src="/image/trun-title.png" />
            <img src="/image/handle.png" />
            <img src="/image/start.png" id="start" style="cursor: pointer; transform: rotate(0deg);"/>
        </div>

        {{--填写个人信息--}}
        <div class="w-info">
            <form id="form" action="" method="post">
                <div class="name info-div">
                    <label style="" class="label"><img src="/image/ic-name.jpg"></label>
                    <input type="text" name="name" placeholder="姓名">
                </div>
                <div class="phone info-div">
                    <label style="" class="label"><img src="/image/ic-phone.jpg" style="  width: 12%;"></label>
                    <input type="text" name="phone" placeholder="电话">
                </div>
                <div class="province info-div">
                    <label style="" class="label"><img src="/image/ic-province.jpg" style="  width: 27px;"></label>
                    <select name="province" class="provincelist" style="  width: 65%;">
                        <option selected value="">省份</option>
                        @foreach($province as $val)
                            <option value="{{$val->province_code}}">{{$val->province_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="city info-div">
                    <label style="" class="label"><img src="/image/ic-city.jpg" style="  width: 25px;"></label>
                    <select name="city" class="citylist" style="  width: 65%;">
                        <option selected value="">城市</option>
                    </select>
                </div>
                <div class="position info-div">
                    <label style="" class="label"><img src="/image/ic-shop.jpg" style="  width: 20px;"></label>
                    <select name="shop" class="shop" style="width: 70%;">
                        <option selected value="">意向销售店</option>
                    </select>
                </div>
                <div class="type info-div">
                    <label style="" class="label"><img src="/image/ic-type.jpg" style="  width: 25px;"></label>
                    <select name="type">
                        <option selected value="">意向车型</option>
                        @foreach($modles as $val)
                            <option value="{{$val->modle_code}}">{{$val->modle_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="time info-div">
                    <label style="" class="label"><img src="/image/ic-time.jpg" style="  width: 25px;"></label>
                    <select name="time">
                        <option selected value="">购车时间</option>
                        @foreach($time as $val)
                            <option value="{{$val}}">{{$val}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="submit"><img src="/image/submit.png"/></div>
                <input type="hidden" name="nums" value="{{ $nums}}">
            </form>
        </div>
        {{--获取邀请码--}}
        <div class="getcode">
            <div class="p1" data-type="1"></div>
            <div class="p2" data-type="4"></div>
            <div class="p3" data-type="5"></div>
            <div class="p4" data-type="2"></div>
            <div class="p5" data-type="3"></div>
        </div>
        {{--获得奖品详情页--}}
        <div class="p-success" id="p-success" style="display: none;">
            <div class="p-s-img"><img src="/image/p_bg.png"></div>
            <div class="ps_close"></div>
            <p class="ps-title"></p>
            <div class="ps-img"><img src=""/> </div>
            <div class="ps-way"></div>
        </div>
        {{--试驾--}}
        <a href="https://mall.gac-toyota.com.cn/Activities?ID=e79f3b6f-6f0d-49a7-b019-a6e801231340" class="leiling" ></a>
        <a href="https://mall.gac-toyota.com.cn/Activities?ID=3811c19d-f2dc-4b08-8aff-a730012bdcc5" class="zhixiang" ></a>
        {{--回到首页--}}
        <a href="https://mall.gac-toyota.com.cn/" class="back_index" ></a>
        {{--弹窗--}}
        <div class="info-error">
            <img src="/images/error.png"/>
            <p></p>
            <div class="in-close"></div>
        </div>
        <div class="alert">
            <div class="success" style="display: none">
                {{--<div class="al-img"><img src="/images/alert.png"/></div>--}}
                <p class="tishi"></p>
                <p class="content"></p>
                <a href="" class="al-href"></a>
            </div>
            <div class="error" style="display: none">
                {{--<div class="al-img"><img src="/images/noget.png"/></div>--}}
                <a href="/#form" class="a-info"></a>
            </div>
            <div class="error2" style="display: none">
                {{--<div class="al-img"><img src=""/></div>--}}
                <a class="d-close"></a>
            </div>
        </div>
        <div class="back">
        </div>
    </div>
    <script type="text/javascript" src="/js/jQueryRotate.2.2.js"></script>
    <script type="text/javascript">
        var status = "{{$status}}";
        var storage = window.localStorage;
        var _token = "{{ csrf_token() }}";
        var prize = '';
//        console.log();
        var width = $(window).width()/2-125;
        function show_error(message) {
            $('.info-error p').text(message);
            $('.info-error,.back').show();
        }
        function error2(src) {
            $(".error2").css("background",'url('+src+') no-repeat 0 0');
            $(".error2").css("background-size",'100%');
            $(".error2,.back").show();
        }
        function show(data) {
            $(".tishi").text(data['tishi']);
            $(".content").text(data['content']);
            $(".al-href").text(data['al-href']);
            $(".al-href").attr("href", data['href']);
            $(".success,.back").show();
        }

//        var data = new Array();
//        data['tishi'] = '人品爆发';
//        data['content'] = '获得';
//        data['href'] = '/#head';
//        data['al-href'] = '查看礼品 >>';
//        show(data);
        $(function () {
            $(".error2,.error,.success").css("left",width+'px');
            $(".in-close").click(function () {
                $('.info-error,.back').hide();
            });
            $(".ps_close").click(function () {
                $(".p-success,.back").hide();
            });
            $(".al-href").click(function () {
                var href = $(this).attr("href");
                if (href == '/#head') {
                    $(".p-success").show();
                }
                $(".success,.back").hide();
                return false;
            });
            $(".a-info").click(function () {
                $(".error,.back").hide();
            });
            $(".d-close").click(function () {
                $(".error2,.back").hide();
            });

            $("#start").click(function () {
                lottery();
            });

            $(".getcode div").click(function () {
                var prize = $(this).data("type");//获取奖品编号
                $.ajax({
                    url: '/sendcode/' + prize,
                    async: false,
                    type: "GET",
                    data: {},
                    dataType: "json",
                    error: function () {
                    },
                    success: function (data) {
                        if(data.code=='2'){
                            show_error("你没有获得该奖品");
                        }else{
                            show_error("核销码已发送到您的手机");
                        }
                    }
                });
            });

            /**
             * 获取城市列表
             */
            $(".provincelist").change(function () {
                var code = $(this).val();
                $.ajax({
                    url: '/city/' + code,
                    async: false,
                    type: "GET",
                    data: {},
                    dataType: "json",
                    error: function () {

                    },
                    success: function (data) {
                        var str = '';
                        $.each(data, function (index, value) {
                            str += '<option value="' + value.city_code + '">' + value.city_name + '</option>';
                        });
                        $(".citylist").append(str);
                    }
                });
            });
            /**
             * 获取门店列表
             */
            $(".citylist").change(function () {
                var code = $(this).val();
                $.ajax({
                    url: '/shop/' + code,
                    async: false,
                    type: "GET",
                    data: {},
                    dataType: "json",
                    error: function () {
                    },
                    success: function (data) {
                        var str = '';
                        $.each(data, function (index, value) {
                            str += '<option value="' + value.dealer_code + '">' + value.dealer_name + '</option>';
                        });
                        $(".shop").append(str);
                    }
                });
            });

            /**
             * 个人信息提交
             */
            $(".submit img").click(function () {
                var name = $('input[name=name]').val();
                var phone = $('input[name=phone]').val();
                var province = $('select[name=province]').val();
                var city = $('select[name=city]').val();
                var shop = $('select[name=shop]').val();
                var type = $('select[name=type]').val();
                var time = $('select[name=time]').val();
                var nums = $('input[name=nums]').val();
                if (nums != '0') {
                    show_error("你已经填写过信息了");
                    return;
                }
                if (name == '') {
                    show_error("请输入姓名");
                    return;
                }
                if (phone == '') {
                    show_error("请输入手机号");
                    return;
                }
                if (phone.length != 11 || !phone.match(/^1[3|5|7|8][0-9]{9}$/)) {
                    show_error("请输入正确的手机号");
                    return;
                }
                if (province == '') {
                    show_error("请选中省份");
                    return;
                }
                if (city == '') {
                    show_error("请选择城市");
                    return;
                }
                if (shop == '') {
                    show_error("请选择意向销售店");
                    return;
                }
                if (type == '') {
                    show_error("请选择意向车型");
                    return;
                }
                if (time == '') {
                    show_error("请输入意向购车时间");
                    return;
                }
                if (name != '' && phone != '' && province != '' && city != '' && shop != '' && type != '' && time != '') {
                    $.ajax({
                        url: '/handle_info',
                        async: false,
                        type: "post",
                        data: {
                            'name': name,
                            'phone': phone,
                            'province': province,
                            "city": city,
                            'shop': shop,
                            'type': type,
                            'time': time,
                            '_token': _token,
                            'nums': prize
                        },
                        dataType: "json",
                        error: function () {

                        },
                        success: function (data) {
                            switch (data.code) {
                                case '1':
                                    show_error("信息提交成功，再去转一次");
//                                    location.href="/#head";
                                    break;
                                case '2':
                                    show_error("手机号已经被注册");
                                    break;
                                case '3':
                                    $(".p-success").show();
                                    location.href = '/#head';
                                    break;

                            }

                        }
                    });
                }
            });

        });
        /**
         * 开始抽奖   请求获取什么奖品
         */
        function lottery() {
            if (status) {
                $.ajax({
                    type: 'get',
                    url: '/start/1',
                    dataType: 'json',
                    cache: false,
                    error: function () {
                    },
                    success: function (json) {
//                    $("#start").unbind('click').css("cursor", "default");
                        if (json.err == '0') {
                            error2("/images/nonum.png");
                        } else {
                            var a = json.angle; //角度
                            var p = json.type; //奖项
                            var pid = json.id; //奖项
                            $("#start").rotate({
                                duration: 3000, //转动时间
                                angle: 0,
                                animateTo: 1800 + a, //转动角度
                                easing: $.easing.easeOutSine,
                                callback: function () {
                                    //判断是第几次没抽中  第一次去填写信息页面
                                    var a = storage.a ? storage.a : 0;
                                    if (pid > 5) {
                                        storage.a = Number(a) + 1;
                                        if (storage.a == '1') {//第一次没有中
                                            $(".error").show();
                                        } else {
                                            error2("/images/noget2.png");
                                        }
                                    } else {
                                        if (storage.a == '1') {
                                            var data = new Array();
                                            data['tishi'] = '人品爆发';
                                            data['content'] = '获得' + p;
                                            data['href'] = '/#head';
                                            data['al-href'] = '查看礼品 >>';
                                            show(data);
                                        } else {
                                            var data = new Array();
                                            data['tishi'] = '人品爆发';
                                            data['content'] = '获得' + p;
                                            data['href'] = '/#start';
                                            data['al-href'] = '留资领奖品 >>';
                                            show(data);
                                        }

                                        $(".ps-title").html("恭喜您<br /> 获得价值" + json.prize.name);
                                        $(".ps-img img").attr('src', '/images/' + json.prize.path);
                                        $(".ps-way").text("兑换方式:" + json.prize.way);
                                        $(".ps-img").addClass("ps-img" + pid)

                                        prize = '1';

                                    }
                                }
                            });
                        }
                    }
                });
            } else {
                error2("/images/nonum.png");
            }
        }
    </script>

@endsection